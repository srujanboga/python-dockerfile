""" Creating a template for dockerfile """
import sys
import json
import os
import shutil
import fileinput
import yaml
 
def yaml_to_json(ymlpath, jsonpath):
    """ This function is to update the yaml to json """
    # Checking there is a file name passed
    if len(ymlpath) > 1:
        # Checking is the file exists in the given path
        if os.path.exists(ymlpath):
            #Opening the file
            #source_file = open(ymlpath, "r", encoding='UTF-8')
            with open(ymlpath, "r", encoding='UTF-8') as source_file:
                source_content = yaml.safe_load(source_file)
            #source_file.close()
        # Failing if the file isn't found
        else:
            print("ERROR: " + ymlpath + " not found")
            sys.exit(1)
    # No file, no usage
    else:
        print("Usage: yaml2json.py <source_file.yaml> [target_file.json]")
    # Converting the YAML content to JSON
    output = json.dumps(source_content)
    # If no target file
    if len(jsonpath) < 3:
        print("ERROR: Required two arguments")
    # Checking is file already exists
    elif os.path.exists(jsonpath):
        print("ERROR: " + jsonpath + " already exists")
        sys.exit(1)
    # Otherwise write to the specified file
    else:
        #target_file = open(jsonpath, "w", encoding='UTF-8')
        with open(jsonpath, "w", encoding='UTF-8') as target_file:
            target_file.write(output)
        # target_file.close()
    return jsonpath
 
#Define the files location
F_JS_PATH = "./"
JS_FILENAME_CONVERT = 'versions.json'
TEMPLATE_FILENAME = 'Dockerfile'
YAML_FILENAME = 'versions.yml'
 
if os.path.exists(JS_FILENAME_CONVERT):
    os.remove(JS_FILENAME_CONVERT)
 
JSON_FILENAME = yaml_to_json(YAML_FILENAME ,JS_FILENAME_CONVERT)
print(JSON_FILENAME)
 
#if not os.path.exists('./src'): os.makedirs('./src')
# Reading from json file
 
with open('./'+JSON_FILENAME, 'r', encoding='UTF-8') as openfile:
    json_object = json.load(openfile)
# Print the entire json file
print(json_object)
 
#Creating the subdirectories
def recursive_items(dictionary,subdirectory_path):
    """ This function is to create the subdirectories """
    for json_key, json_value in dictionary.items():
        #if type(json_value) is dict:
        if isinstance(json_value, dict):
            os.makedirs(subdirectory_path+'/', exist_ok=True)
            yield from recursive_items(json_value,subdirectory_path+'/'+json_key)
        else:
            os.makedirs(subdirectory_path+'/', exist_ok=True)
            shutil.copy('./'+TEMPLATE_FILENAME, subdirectory_path+'/')
            yield (json_key, json_value,subdirectory_path+'/')
 
# It is reading the json file
for key1, value1, src_directory in recursive_items(json_object,'./src/'):
    pass
 
# updating the template files in sub directories
def replace_file(dictionary, subdir_path):
    """ This function is to update the template files in sub directories"""
    for json_replace_key, json_replace_value in dictionary.items():
        #if loop is checking whether there subfolders exists
        #if type(json_replace_value) is dict:
        if isinstance(json_replace_value, dict):
            return replace_file(json_replace_value,subdir_path+'/'+json_replace_key)
        #if subfolder doesnt exists then
        #it is going to dockerfile.common
        #replace the json_replace_values by matching the json_replace_key.
        else:
            input_file = subdir_path+'/'+TEMPLATE_FILENAME
            with fileinput.FileInput(input_file, inplace=True) as file_object:
                for line in file_object:
                    print(line.replace('${'+str(json_replace_key)+'}',
                                       str(json_replace_value)), end='')
            file_object.close()
 
COUNT = 0
# checking the number of json_replace_keys 17,18,19
for key in json_object.keys():
    replace_file(json_object[key],'./src/'+str(key))
    COUNT+=1
