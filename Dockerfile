ARG BASE_IMAGE_TAG
 
FROM openjdk:${BASE_IMAGE_TAG}
 
ARG maven_version="${maven_version}"
ARG gradle_version="${gradle_version}"
ARG jdk_version="${jdk_version}"
ARG zip_version="${zip_version}"
ARG jq_version="${jq_version}"
 
LABEL maintainer="Ally DevOps Product Team AllyDevOpsProductTeam@Ally.com" \
    com.ally.jdk.version=${jdk_version} \
    com.ally.maven.version=${maven_version} \
    com.ally.gradle.version=${gradle_version} \
    com.ally.zip.version=${zip_version} \
    com.ally.jq.version=${jq_version}
 
ENV ALLY_PRIVATE_CA_PATH="/usr/local/share/ca-certificates/ally-ca.crt"
ENV AWS_PRIVATE_CA_PATH="/usr/local/share/ca-certificates/ally-aws-private-ca.crt"
 

